package nl.johannisk.day1;

import nl.johannisk.AdventDay;

import java.util.List;
import java.util.stream.Collectors;

public class Day1 extends AdventDay {
    public void run() {
        List<String> strings = getPuzzleInputLineByLine("day1.txt");
        List<Integer> integers = strings.stream()
                .map(Integer::parseInt)
                .collect(Collectors.toList());
        part1(integers);
        part2(integers);
    }

    private void part1(List<Integer> integers) {
        for(Integer i : integers) {
            for(Integer j : integers) {
                if(i + j == 2020) {
                    System.out.println("Part1: " + (i * j));
                    return;
                }
            }
        }
    }

    private void part2(List<Integer> integers) {
        for(Integer i : integers) {
            for(Integer j : integers) {
                for(Integer k : integers) {
                    if(i + j + k == 2020) {
                        System.out.println("Part 2: " + (i * j * k));
                        return;
                    }
                }
            }
        }
    }
}
