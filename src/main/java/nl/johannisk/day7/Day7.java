package nl.johannisk.day7;

import nl.johannisk.AdventDay;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Day7 extends AdventDay {

    private Map<String, Bag> bags = new HashMap<>();
    int count = 0;

    public void run() {
        List<String> strings = getPuzzleInputLineByLine("day7.txt");

        for (String string : strings) {
            parseBag(string);
        }

        System.out.println("Part1: " + (bags.values().stream()
                .filter(b -> !b.getColor().equals("shiny gold"))
                .filter(b -> b.canContain("shiny gold"))
                .count()));
        System.out.println("Part2: " + bags.get("shiny gold").countContents());
    }

    private void parseBag(String string) {
        String[] parts = string.split(" contain ");
        String bagColor = parts[0].substring(0, parts[0].length() - 5);
        Bag outerBag = bags.getOrDefault(bagColor, new Bag(bagColor));
        bags.putIfAbsent(bagColor, outerBag);
        String[] contentParts = parts[1].split(", ");
        for (String item : contentParts) {
            String quantityString = item.substring(0, item.indexOf(" "));
            if (quantityString.equals("no"))
                continue;
            int quantity = Integer.parseInt(quantityString);
            String color = item.substring(item.indexOf(" ") + 1, item.indexOf(" bag"));
            Bag innerBag = bags.getOrDefault(color, new Bag(color));
            outerBag.addContent(quantity, innerBag);
            bags.putIfAbsent(color, innerBag);
        }
    }
}
