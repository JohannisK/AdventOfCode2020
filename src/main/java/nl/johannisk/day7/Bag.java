package nl.johannisk.day7;

import java.util.*;

public class Bag {
    private final String color;
    private final Map<Bag, Integer> contents = new HashMap<>();

    public Bag(String color) {
        this.color = color;
    }

    public String getColor() {
        return color;
    }

    public void addContent(int quantity, Bag contentBag) {
        contents.put(contentBag, quantity);
    }

    public boolean canContain(String color) {
        return this.color.equals(color) || contents.keySet().stream().anyMatch(s -> s.canContain(color));
    }

    public int countContents() {
        int count = 0;
        for(Map.Entry<Bag, Integer> entry : contents.entrySet()) {
            count += entry.getValue() + entry.getKey().countContents() * entry.getValue();
        }
        return count;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Bag bag = (Bag) o;
        return Objects.equals(color, bag.color);
    }

    @Override
    public int hashCode() {
        return Objects.hash(color);
    }
}
