package nl.johannisk.day2;

import nl.johannisk.AdventDay;

import java.util.List;

public class Day2 extends AdventDay {
    public void run() {
        List<String> strings = getPuzzleInputLineByLine("day2.txt");
        long countDay1 = strings.stream()
                .map(PasswordEntry::new)
                .filter(p -> p.validate((lowerbound, upperbound, character, password) -> {
                    int count = 0;
                    for(char c : password.toCharArray()) {
                        if(c == character) {
                            count++;
                        }
                    }
                    return (count >= lowerbound && count <= upperbound);
                }))
                .count();

        long countDay2 = strings.stream()
                .map(PasswordEntry::new)
                .filter(p -> p.validate((lowerbound, upperbound, character, password) ->
                        (password.charAt(lowerbound - 1) == character || password.charAt(upperbound - 1) == character) &&
                                password.charAt(lowerbound - 1) != password.charAt(upperbound - 1)))
                .count();

        System.out.println("Part1: " + countDay1);
        System.out.println("Part2: " + countDay2);
    }
}
