package nl.johannisk.day2;

public class PasswordEntry {
    private final int lowerbound;
    private final int upperbound;
    private final char character;
    private final String password;

    public PasswordEntry(String line) {
        String[] parts = line.split(" ");
        lowerbound = Integer.parseInt(parts[0].substring(0, parts[0].indexOf("-")));
        upperbound = Integer.parseInt(parts[0].substring(parts[0].indexOf("-") + 1));
        character = parts[1].charAt(0);
        password = parts[2];
    }

    public boolean validate(PasswordValidator validator) {
        return validator.validate(lowerbound, upperbound, character, password);
    }
}
