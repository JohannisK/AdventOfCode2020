package nl.johannisk.day2;

@FunctionalInterface
public interface PasswordValidator {
    boolean validate(int lowerbound, int upperbound, char character, String password);
}
