package nl.johannisk.day6;

import java.util.ArrayList;
import java.util.List;

public class Group {

    List<Answer> answers = new ArrayList<>();

    public void add(String s) {
        answers.add(new Answer(s));
    }

    public List<Answer> getAnswers() {
        return answers;
    }

    public int getCountAllYes() {
        int count = 0;
        for(char c = 'a'; c <= 'z'; c++) {
            if(shouldIncrementForCharacter(c))
                count++;
        }
        return count;
    }

    private boolean shouldIncrementForCharacter(char c) {
        for(Answer a : answers) {
            if(!a.getCharacters().contains(c))
                return false;
        }
        return true;
    }
}
