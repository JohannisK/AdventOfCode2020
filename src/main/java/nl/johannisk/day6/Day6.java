package nl.johannisk.day6;

import nl.johannisk.AdventDay;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Day6 extends AdventDay {

    List<Group> groups = new ArrayList<>();

    public void run() {
        List<String> strings = getPuzzleInputLineByLine("day6.txt");
        Group group = new Group();
        for (String s : strings) {
            if (s.isBlank()) {
                groups.add(group);
                group = new Group();
            } else {
                group.add(s);
            }
        }
        groups.add(group);

        System.out.println("Part 1: " + groups.stream()
                .map(Group::getAnswers)
                .map(s -> s.stream()
                        .flatMap(a -> a.getCharacters().stream())

                )
                .map(s -> s.distinct())
                .map(s -> s.count())
                .reduce(0L, Long::sum));


        System.out.println("Part 2: " + groups.stream()
                .map(Group::getCountAllYes)
                .reduce(0, Integer::sum));
    }
}
