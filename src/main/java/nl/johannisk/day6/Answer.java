package nl.johannisk.day6;

import java.util.ArrayList;
import java.util.List;

public class Answer {

    List<Character> characters = new ArrayList<>();

    public Answer(String s) {
        for(char c : s.toCharArray()) {
            characters.add(c);
        }
    }

    public List<Character> getCharacters() {
        return characters;
    }
}
