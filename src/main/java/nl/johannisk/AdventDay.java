package nl.johannisk;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

public abstract class AdventDay {

    public abstract void run();

    protected final List<String> getPuzzleInputLineByLine(String name) {
        List<String> strings = null;
        try {
            URL resource = Runner.class.getClassLoader().getResource(name);
            strings = Files.readAllLines(Path.of(resource.toURI()));
        } catch (IOException | URISyntaxException e) {
            e.printStackTrace();
        }
        return strings;
    }
}
