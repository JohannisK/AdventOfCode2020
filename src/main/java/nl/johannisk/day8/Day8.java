package nl.johannisk.day8;

import nl.johannisk.AdventDay;

import java.util.List;
import java.util.stream.Collectors;

public class Day8 extends AdventDay {
    public void run() {
        List<String> strings = getPuzzleInputLineByLine("day8.txt");
        List<OppCode> instructions = strings.stream()
                .map(OppCode::new)
                .collect(Collectors.toList());
        OppCodeComputer oppCodeComputer = new OppCodeComputer(instructions);

        System.out.println("Part1: " + oppCodeComputer.debug());
        System.out.println("Part2: " + oppCodeComputer.hotfix());
    }
}
