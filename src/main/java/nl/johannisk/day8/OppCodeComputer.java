package nl.johannisk.day8;

import java.util.ArrayList;
import java.util.List;
import java.util.function.IntFunction;

public class OppCodeComputer {
    private List<OppCode> instructions;
    private int currentInstruction = 0;
    private int accumulator = 0;

    private List<Integer> executedInstructions = new ArrayList<>();

    public OppCodeComputer(List<OppCode> instructions) {
        this.instructions = instructions;
    }

    public void update(IntFunction<Integer> updateInstruction, IntFunction<Integer> updateAccumulator) {
        currentInstruction = updateInstruction.apply(currentInstruction);
        accumulator = updateAccumulator.apply(accumulator);
    }

    public int debug() {
        while (!executedInstructions.contains(currentInstruction)) {
            executedInstructions.add(currentInstruction);
            instructions.get(currentInstruction).execute(this);
        }
        return accumulator;
    }

    public int hotfix() {

        List<Integer> fixedInstructions = new ArrayList<>();
        boolean didFix = false;

        while (currentInstruction < instructions.size()) {
            if (executedInstructions.contains(currentInstruction)) {
                currentInstruction = 0;
                accumulator = 0;
                executedInstructions = new ArrayList<>();
                didFix = false;
            }
            executedInstructions.add(currentInstruction);

            OppCode oppCode = instructions.get(currentInstruction);
            if ((oppCode.getOperation().equals("jmp") || oppCode.getOperation().equals("nop")) && !fixedInstructions.contains(currentInstruction) && !didFix) {
                fixedInstructions.add(currentInstruction);
                didFix = true;
                if (oppCode.getOperation().equals("nop")) {
                    oppCode.execute(this, "jmp");
                } else {
                    oppCode.execute(this, "nop");
                }
            } else {
                oppCode.execute(this);
            }
        }
        return accumulator;
    }
}
