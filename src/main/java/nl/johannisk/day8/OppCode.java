package nl.johannisk.day8;

import java.util.function.IntFunction;

public class OppCode {

    private String operation;
    private int value;

    public OppCode(String oppcode) {
        String[] parts = oppcode.split(" ");
        operation = parts[0];
        value = Integer.parseInt(parts[1]);
    }

    public void execute(OppCodeComputer oppCodeComputer) {
        execute(oppCodeComputer, operation);
    }

    public void execute(OppCodeComputer oppCodeComputer, String operation) {
        IntFunction<Integer> instructionUpdate;
        IntFunction<Integer> accumulatorUpdate;
        switch (operation) {
            case "jmp":
                instructionUpdate = currentInstruction -> currentInstruction + value;
                accumulatorUpdate = accumulator -> accumulator;
                break;
            case "nop":
                instructionUpdate = currentInstruction -> currentInstruction + 1;
                accumulatorUpdate = accumulator -> accumulator;
                break;
            case "acc":
                instructionUpdate = currentInstruction -> currentInstruction + 1;
                accumulatorUpdate = accumulator -> accumulator + value;
                break;
            default:
                throw new RuntimeException("Could not parse opp: " + operation);
        }
        oppCodeComputer.update(instructionUpdate, accumulatorUpdate);
    }

    public String getOperation() {
        return operation;
    }
}
