package nl.johannisk.day4;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;

public class Passport {

    private Map<String, String> values = new HashMap<>();
    private Map<String, Predicate<String>> requiredFieldValidators = new HashMap<>();
    private List<String> validEyeColors = Arrays.asList("amb", "blu", "brn", "gry", "grn", "hzl", "oth");

    public Passport() {
        requiredFieldValidators.put("byr", s -> isBetween(1920, 2002, s));
        requiredFieldValidators.put("iyr", s -> isBetween(2010, 2020, s));
        requiredFieldValidators.put("eyr", s -> isBetween(2020, 2030, s));
        requiredFieldValidators.put("hgt", s -> validateHeight(s));
        requiredFieldValidators.put("hcl", s -> s.matches("#[0-9A-Fa-f]{6}"));
        requiredFieldValidators.put("ecl", s -> validEyeColors.contains(s));
        requiredFieldValidators.put("pid", s -> s.matches("[0-9]{9}"));
    }

    private boolean validateHeight(String s) {
        String type = s.substring(s.length() - 2);
        String value = s.substring(0, s.length() - 2);
        if("cm".equals(type)) {
            return isBetween(150,193, value);
        } else if("in".equals(type)) {
            return isBetween(59,76, value);
        } else {
            return false;
        }
    }

    private boolean isBetween(int min, int max, String s) {
        if(s.isBlank())
            return false;
        int value = Integer.parseInt(s);
        return value >= min && value <= max;
    }

    public void add(String key, String value) {
        values.put(key, value);
    }

    public boolean containsAllRequiredFields() {
        for (String key : requiredFieldValidators.keySet()) {
            if (!values.containsKey(key)) {
                return false;
            }
        }
        return true;
    }

    public  boolean isValid() {
        for (Map.Entry<String, Predicate<String>> pair : requiredFieldValidators.entrySet()) {
            if (!values.containsKey(pair.getKey())) {
                return false;
            } else {
                if(!pair.getValue().test(values.get(pair.getKey()))){
                    return false;
                }
            }
        }
        return true;
    }
}
