package nl.johannisk.day4;

import nl.johannisk.AdventDay;

import java.util.ArrayList;
import java.util.List;

public class Day4 extends AdventDay {

    List<Passport> passports = new ArrayList<>();

    public void run() {
        List<String> strings = getPuzzleInputLineByLine("day4.txt");
        Passport passport = new Passport();
        for(String line : strings) {
            if(line.isBlank()) {
                passports.add(passport);
                passport = new Passport();
            } else {
                String[] pairs = line.split(" ");
                for(String pair : pairs) {
                    String[] keyValue = pair.split(":");
                    passport.add(keyValue[0], keyValue[1]);
                }
            }
        }
        passports.add(passport);
        System.out.println("Part 1: " + passports.stream()
                .filter(Passport::containsAllRequiredFields)
                .count());

        System.out.println("Part 2: " + passports.stream()
                .filter(Passport::isValid)
                .count());
    }

}
