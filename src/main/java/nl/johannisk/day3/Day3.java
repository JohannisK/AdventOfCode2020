package nl.johannisk.day3;

import nl.johannisk.AdventDay;

import java.util.List;

public class Day3 extends AdventDay {

    int width;
    int height;
    private char[][] slope;

    public void run() {
        List<String> strings = getPuzzleInputLineByLine("day3.txt");
        createSlope(strings);
        int first = countTrees(slope, 1, 1);
        int second = countTrees(slope, 3, 1);
        int third = countTrees(slope, 5, 1);
        int fourth = countTrees(slope, 7, 1);
        int fifth = countTrees(slope, 1, 2);

        System.out.println("Part1: " + second);
        System.out.println("Part2: " + first * second * third * fourth * fifth);

    }

    private int countTrees(char[][] slope, int xcomp, int ycomp) {
        int x = 0;
        int y = 0;
        int treeCount = 0;
        while(y < height) {
            if(x >= width) {
                x -= width;
            }
            if(slope[y][x] == '#') {
                treeCount++;
            }
            x += xcomp;
            y += ycomp;
        }
        return treeCount;
    }

    private void createSlope(List<String> strings) {
        height = strings.size();
        width = strings.get(0).length();
        slope = new char[height][width];
        for(int y = 0; y < strings.size(); y++) {
            char[] chars = strings.get(y).toCharArray();
            slope[y] = chars;
        }
    }


}
