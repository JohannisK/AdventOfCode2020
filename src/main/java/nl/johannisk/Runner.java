package nl.johannisk;

import nl.johannisk.day1.Day1;
import nl.johannisk.day2.Day2;
import nl.johannisk.day3.Day3;
import nl.johannisk.day4.Day4;
import nl.johannisk.day5.Day5;
import nl.johannisk.day6.Day6;
import nl.johannisk.day7.Day7;
import nl.johannisk.day8.Day8;

import java.lang.reflect.InvocationTargetException;

public class Runner {
    public static void main(String[] args) throws NoSuchMethodException, IllegalAccessException, InstantiationException, InvocationTargetException {
        run(Day1.class);
        run(Day2.class);
        run(Day3.class);
        run(Day4.class);
        run(Day5.class);
        run(Day6.class);
        run(Day7.class);
        run(Day8.class);
    }

    public static void run(Class<? extends AdventDay> clazz) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        System.out.println(">> " + clazz.getSimpleName() + ": ");
        clazz.getDeclaredConstructor().newInstance().run();
        System.out.println();
    }
}
