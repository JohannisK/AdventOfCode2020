package nl.johannisk.day5;

import java.util.Objects;

public class BoardingPass implements Comparable<BoardingPass>{

    private int row;
    private int column;
    private int seatId;

    public BoardingPass(String string) {
        row = Integer.parseInt(string.substring(0,7).replaceAll("F", "0").replaceAll("B", "1"),2);
        column = Integer.parseInt(string.substring(7,string.length()).replaceAll("L", "0").replaceAll("R", "1"),2);
        seatId = row * 8 + column;
    }

    public BoardingPass(int i) {
        this.seatId = i;
    }

    public int getSeatId() {
        return seatId;
    }

    @Override
    public int compareTo(BoardingPass that) {
        if(this.seatId > that.seatId)
            return 1;
        if(this.seatId < that.seatId)
            return -1;
        return 0;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BoardingPass that = (BoardingPass) o;
        return seatId == that.seatId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(seatId);
    }
}
