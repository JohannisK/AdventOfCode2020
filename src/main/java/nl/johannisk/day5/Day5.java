package nl.johannisk.day5;

import nl.johannisk.AdventDay;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

public class Day5 extends AdventDay {
    private List<BoardingPass> boardingPasses = new ArrayList<>();

    public void run() {
        List<String> strings = getPuzzleInputLineByLine("day5.txt");
        for(String s : strings) {
            boardingPasses.add(new BoardingPass(s));
        }

        boardingPasses.sort(BoardingPass::compareTo);
        int min = boardingPasses.get(0).getSeatId();
        int max = boardingPasses.get(boardingPasses.size() - 1).getSeatId();
        System.out.println("Part1: " + max);
        IntStream.range(min,max)
                .filter(i -> !boardingPasses.contains(new BoardingPass(i)))
                .forEach(s -> System.out.println("Part2: " + s));
    }

}
